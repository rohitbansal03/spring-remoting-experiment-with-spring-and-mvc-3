<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
    <title>Accounts</title>
</head>
<body>

    <h1>Account Management</h1>

    <form:form modelAttribute="account" method="post" action="accounts/new">
        <form:label path="name">Account name:</form:label>
        <form:input path="name"/>
        <input type="submit" name="Create Account" />
    </form:form>

    <h2>Accounts:</h2>
    <table>
    <c:forEach items="${accounts}" var="account" varStatus="i">
        <tr>
            <td>${i.count}</td>
            <td><c:out value="${account.name}" /></td>
        </tr>
    </c:forEach>
    </table>

</body>
</html>