package experiment.springremoting;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * User: peter.nguyen
 * Date: 22/11/12
 * Time: 10:03 PM
 */
public class AccountServiceImpl implements AccountService {

    private static List<Account> accounts = new CopyOnWriteArrayList<Account>();

    @Override
    public boolean insertAccount(Account account) {
        return accounts.add(account);
    }

    @Override
    public List getAccounts() {
        return accounts;
    }
}
